<?php

/**
 * Kirki Framework
 * ==============================================================================
 */
Kirki::add_config( 'kirki_custom_config', array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

/**
 * Painéis
 * ==============================================================================
 */
Kirki::add_panel( 'rodape', array(
    'priority'    => 10,
    'title'       => esc_attr__( 'Rodapé', 'odin' ),
    'description' => esc_attr__( 'Configurações do Rodapé.', 'odin' ),
    'capability'  => 'edit_theme_options'
) );

/**
 * Sessões
 * ==============================================================================
 */
Kirki::add_section( 'rodape', array(
    'title'          => __( 'Rodapé' ),
    'priority'       => 150,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '', // Rarely needed.
) );

/**
 * Campos (separados por Sessões)
 * ==============================================================================
 */

/* Rodapé */
Kirki::add_field( 'kirki_custom_config', array(
	'type'        => 'color',
	'settings'    => 'color_rodape',
	'label'       => __( 'Cor das fontes', 'model' ),
	'description' => esc_attr__( 'Cores para os textos do Rodapé.', 'odin' ),
	'section'     => 'rodape',
	'default'     => '#ffffff',
	'transport'   => 'auto',
	'output'    	=> array(
		array(
			'element'  => 'footer#footer .container-fluid p.credits',
			'property' => 'color'
		),
		array(
			'element'  => 'footer#footer .container-fluid p.credits a',
			'property' => 'color'
		),
	),
) );
Kirki::add_field( 'kirki_custom_config', array(
	'type'			=> 'textarea',
	'settings'		=> 'frase_rodape',
	'label'			=> __( 'Frase personalizada para o Rodapé', 'odin' ),
	'section'		=> 'rodape',
	'default'		=> '',
	'priority'		=> 10,
	'transport'		=> 'auto'
) );
